var express = require('express');
var router = express.Router();
var aws = require('aws-sdk');
var s3 = new aws.S3();

var fileUpload = require('express-fileupload');
router.use(fileUpload());


router.get('/', function(req, res) {
  s3.listBuckets({},function(err,data) {
      if(err) {
          throw err;
      }
      console.log(data);
      res.render('listBuckets', { buckets: data.Buckets});
  });
});

router.get('/:bucket/', function(req, res) {
	console.log("Bucket: " + req.params.bucket);
	params = {
		Bucket: req.params.bucket,
		MaxKeys: 1000
	};
	s3.listObjects(params, function(err, data) {
		if(err) {
			console.log(err, err.stack);
		} else {
			console.log(data);
			res.render('listBucketObjects', {objects: data.Contents});
		}
	})
    /*
     * @TODO - Programa la logica para obtener los objetos de un bucket.
     *         Se debe tambien generar una nueblo templade en jade para presentar
     *         esta información. Similar al que lista los Buckets.
     */
    
});

router.get('/:bucket/:key', function(req, res) {
    var params = {
		Bucket: req.params.bucket,
		Key: req.params.key
	};
	s3.getObject(params, function(err, data) {
		if(err) {
			console.log(err, err.stack);
		} else {
			console.log(data);
			res.type(data.ContentType);
			res.send(data.Body);
		}
	})
    /*
     * @TODO - Programa la logica para obtener un objeto en especifico
     * es importante a la salida enviar el tipo de respuesta y el contenido
     * 
     * Ejemplo de esto:
     *     res.type(...) --> String de content-type
     *     res.send(...) --> Buffer con los datos.
     */    
});


router.post('/', function(req,res) {
	var params = {
		Bucket: "unNombreFijo",
		CreateBucketConfiguration: {
			LocationConstraint: 'us-west-2'
		}
	};
	
	s3.createBucket(params, function(err, data) {
		var addFile = true;
		if(err) {
			if(err.code == 'BucketAlreadyOwnedByYou') {
				console.log("El bucket ya existe");
			} else {
				console.log(err, err.stack);
				addFile = false;
			}
		}
	
		if(addFile) {
			var params = {
				Bucket: bucketName,
				Key: 'archivo01.txt',
				Body: 'Hola mundo'
			};
	
			s3.putObject(params, function(err, data) {
				if(err) {
					console.log(err, err.stack);
				} else {
					console.log('Archivo agregado...ETag: ' + data.ETag);
				}
			});
		}
	});
    /*
     * @TODO - Programa la logica para crear un Bucket.
    */
});

router.post('/:bucket', function(req,res) {

    /*
     * @TODO - Programa la logica para crear un nuevo objeto.
     * TIPS:
     *  req.files contiene todo los archivos enviados mediante post.
     *  cada elemento de files contiene multiple información algunos campos
     *  importanets son:
     *      data -> Buffer con los datos del archivo.
     *      name -> Nombre del archivo original
     *      mimetype -> tipo de archivo.
     *  el conjunto files dentro del req es generado por el modulo 
     *  express-fileupload
     *  
    */
   var params = {
	Bucket: req.params.bucket,
	Key: req.files[''].name,
	mimetype: req.files[''].mimetype,
	Body: req.files[''].data
};

s3.upload(params, function(err, data) {
   console.log(err, data);
   if(err) {
	res.status(200);
	res.send("File uploaded");
   } else {
	res.status(200);
	res.send("File uploaded");
   }
});
	 
});

module.exports = router;
